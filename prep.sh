#!/usr/bin/sh

pandoc Readme.md -t html5 -H style.txt -o Readme.html
zip -r midiasm.zip examples/ midiasm.tcl Readme.md Readme.html
