# if != 0, user interpreter will be started in safe mode
set UNSAFE 0

# facilitates communication between user and master interpreters
namespace eval Comms {

    namespace export {*}{
        __comms_incr
        __comms_set
        __comms_get
        __comms_append
        __comms_lappend
        __comms_debug
        __comms_slurp
    }
    
    proc __comms_incr {variable {amount 1}} {

        incr ::Comms::$variable $amount

    }

    proc __comms_set {variable value} {

        set ::Comms::$variable $value

    }

    proc __comms_get {variable} {

        return [set ::Comms::$variable]

    }

    proc __comms_append {variable args} {

        append ::Comms::$variable {*}$args

    }

    proc __comms_lappend {variable args} {

        lappend ::Comms::$variable {*}$args

    }

    proc __comms_debug {message} {

        puts stderr $message

    }

    # reading a file from current working directory should be relatively safe
    proc __comms_slurp {filename} {

        set full_filename [file normalize $filename]
        set slurp_dir [file normalize [pwd]]
        
        if [regexp "^$slurp_dir" [file normalize $full_filename]] {

            if [catch {set ifh [open $full_filename r]}] {

                __comms_debug "couldn't open file $filename"
                return ""

            } else {

                set data [read $ifh]
                close $ifh
                return $data

            }

        } else {

            __comms_debug "attemping to slurp file outside of current working tree"
            return ""

        }

    }

}

# import comms stuff into our global namespace
# so master can use comms in the same way the slave does
namespace import Comms::*

# creates a safe interpreter
# evaluates user code in it
# destroys the safe interpreter when done
proc safe_eval {code} {
    
    # destroy old interp
    # (there will only be one if this function failed previously)
    catch {

        interp delete user_interpreter

    }

    # create an interpreter
    if $::UNSAFE {

        interp create user_interpreter

    } else {

        interp create -safe user_interpreter

    }

    # alias comms commands into it
    # so user interp can share data with master interp
    foreach command [info command ::Comms::*] {

        set g_command [regsub {^::Comms::} $command {}]
        user_interpreter alias $g_command $command

    }

    # evaluate code
    user_interpreter eval $code

    # delete the user interpreter
    interp delete user_interpreter

}

# this is run inside the user interpreter before the user code
set startup {

    # comms-related helper commands for the user
    namespace eval Comms {

        namespace export {*}{
            debug
            slurp
            source
        }

        proc debug {message} {

            __comms_debug $message

        }

        proc slurp {filename} {

            return [__comms_slurp $filename]

        }

        proc source {filename} {

            eval [__comms_slurp $filename]

        }

    }

    # this one contains internal midi stuff
    # the user has access to it if needed
    # but it will stay in the Midi:: namespace
    namespace eval Midi {

        # returns a midi "variable length" number
        # big-endian, each byte contains 7 bits of data
        # preceded by a bit saying whether more bytes follow
        proc to_vlength {n} {

            # to hex
            set hex [format %08x $n]

            # the value 0 can't be represented in 0 bits
            # even though 2^0-1 = 0
            # weird...
            if [expr {$n == 0}] {

                return [binary format B* 00000000]

            }

            # to binary (bits)
            set bytes [binary format H* $hex]
            binary scan $bytes B* bits
            regsub {^0+} $bits {} bits

            # to vlength
            set num_bits [string length $bits]
            set num_bytes [expr {int( ($num_bits-1) / 7) + 1}]

            set bits [string reverse "00000000$bits"]
            set vbits ""

            for {set i 0} {$i < $num_bytes} {incr i} {

                set start [expr {$i * 7}]
                set end [expr {$start + 6}]
                set vbits "$vbits[string range $bits $start $end]1"

            }

            set vbits [string reverse $vbits]
            set vbits [string replace $vbits end-7 end-7 0]

            return [binary format B* $vbits]

        }

        # get midi file header
        proc get_header {format_type number_of_tracks ppq} {

            return [binary format a4ISSS \
                "MThd" \
                6 \
                $format_type \
                $number_of_tracks \
                $ppq \
            ]

        }

        # get track chunk header given size of track data
        proc get_track_chunk_header {size} {

            return [binary format a4I \
                "MTrk" \
                $size \
            ]

        }

        # get binary data for a channel event with 1 param
        proc channel_event_1p {event_code time channel p1} {

            return [binary format a*cc \
                [to_vlength $time] \
                [expr {$channel + $event_code}] \
                $p1 \
            ]

        }

        # get binary data for a channel event with 2 params
        proc channel_event_2p {event_code time channel p1 p2} {

            return [binary format a*ccc \
                [to_vlength $time] \
                [expr {$channel + $event_code}] \
                $p1 \
                $p2 \
            ]

        }

        # commonly used events
        proc note_on {time channel note velocity} {

            return [channel_event_2p 144 $time $channel $note $velocity]

        }

        proc note_off {time channel note velocity} {

            return [channel_event_2p 128 $time $channel $note $velocity]

        }

        proc n_touch {time channel key pressure} {

            return [channel_event_2p 160 $time $channel $key $pressure]

        }

        proc c_touch {time channel pressure} {

            return [channel_event_1p 208 $time $channel $pressure]

        }

        proc program_change {time channel instrument} {

            return [channel_event_1p 192 $time $channel $instrument]

        }

        proc cc {time channel type value} {

            return [channel_event_2p 176 $time $channel $type $value]

        }

        proc end_of_track {time} {

            return [binary format a*ccc [to_vlength $time] 255 47 0]

        }

        proc tempo {time bpm} {

            set mpqn [expr {60000000 / $bpm}]
            set bytes [binary format H* [format %08x $mpqn]]

            return [binary format a*ccca3 \
                [to_vlength $time] \
                255 \
                81 \
                3 \
                [string range $bytes 1 end] \
            ]

        }

        proc pitch_bend {time channel amount} {

            incr amount 8192
            binary scan [binary format H* [format %04x $amount]] B* bits

            set bits1 [string range $bits end-6 end]
            set bits2 [string range $bits end-14 end-7]
            set byte1 [binary format B7 $bits1]
            set byte2 [binary format B7 $bits2]

            return [binary format a*caa \
                [to_vlength $time] \
                [expr {$channel + 224}] \
                $byte1 \
                $byte2 \
            ]

        }

        proc sysex {time args} {

            set data [binary format a*c \
                [to_vlength $time] \
                240 \
            ]

            for {set i 0} {$i < [llength $args]} {incr i} {

                set block [lindex $args $i]
                set data [binary format a*ca*c \
                    $data \
                    [string length $block] \
                    $block \
                    247 \
                ]

            }

            return $data

        }

    }

    # this is the main one the user interacts with to create midi
    namespace eval Midi_asm {

        namespace export {*}{
            wait
            note_on
            note_off
            n_touch
            c_touch
            program_change
            cc
            tempo
            pitch_bend
            ppq
            sysex
        }

        proc wait {time} {

            __comms_incr delta $time

        }

        # these are mostly all same as in Midi::
        # except time is implicit (controlled with wait)
        proc note_on {channel note velocity} {

            __comms_append midi_bytes [Midi::note_on [__comms_get delta] $channel $note $velocity]
            __comms_set delta 0

        }

        proc note_off {channel note} {

            __comms_append midi_bytes [Midi::note_off [__comms_get delta] $channel $note 0]
            __comms_set delta 0

        }

        proc n_touch {channel note velocity} {

            __comms_append midi_bytes [Midi::n_touch [__comms_get delta] $channel $note $pressure]
            __comms_set delta 0

        }

        proc c_touch {channel pressure} {

            __comms_append midi_bytes [Midi::c_touch [__comms_get delta] $channel $pressure]
            __comms_set delta 0

        }

        proc program_change {channel instrument} {

            __comms_append midi_bytes [Midi::program_change [__comms_get delta] $channel $instrument]
            __comms_set delta 0

        }

        proc cc {channel type value} {

            __comms_append midi_bytes [Midi::cc [__comms_get delta] $channel $type $value]
            __comms_set delta 0

        }

        proc tempo {bpm} {

            __comms_append midi_bytes [Midi::tempo [__comms_get delta] $bpm]
            __comms_set delta 0

        }

        proc pitch_bend {channel amount} {

            __comms_append midi_bytes [Midi::pitch_bend [__comms_get delta] $channel $amount]
            __comms_set delta 0

        }

        # only use once
        proc ppq {ppq} {

            if {[__comms_get ppq_set] == 0} {

                __comms_set ppq $ppq
                __comms_set ppq_set 1

            }

        }

        proc sysex {args} {

            __comms_append midi_bytes [Midi::sysex [__comms_get delta] {*}$args]
            __comms_set delta 0

        }

    }
    
    # commands to assist with converting musical terminology to midi numbers  
    namespace eval Midi_defs {

        namespace export {*}{
            instrument
            drum
            note
            chord
        }
        
        proc instrument {string} {
        
            set instruments {
                "grand piano"
                "bright grand piano"
                "electric grand piano"
                "honky tonk piano"
                "electric piano"
                "electric piano 2"
                "harpsichord"
                "clavi"
                "celesta"
                "glockenspiel"
                "music box"
                "vibraphone"
                "marimba"
                "xylophone"
                "tubular bells"
                "dulcimer"
                "drawbar organ"
                "percussive organ"
                "rock organ"
                "church organ"
                "reed organ"
                "accordion"
                "harmonica"
                "tango accordion"
                "acoustic guitar"
                "steel guitar"
                "jazz guitar"
                "clean guitar"
                "muted guitar"
                "overdrive guitar"
                "distortion guitar"
                "guitar harmonics"
                "acoustic bass"
                "electric bass"
                "pick bass"
                "fretless bass"
                "slap bass"
                "slap bass 2"
                "violin"
                "viola"
                "cello"
                "contrabass"
                "tremolo strings"
                "pizzicato strings"
                "orchestral harp"
                "timpani"
                "string ensemble"
                "string ensemble 2"
                "synth strings"
                "synth strings 2"
                "choir aahs"
                "voice oohs"
                "synth voice"
                "orchestra hit"
                "trumpet"
                "trombone"
                "tuba"
                "muted trumpet"
                "french horn"
                "brass section"
                "synth brass"
                "synth brass 2"
                "soprano sax"
                "alto sax"
                "tenor sax"
                "bariotone sax"
                "oboe"
                "english horn"
                "bassoon"
                "clarinet"
                "piccolo"
                "flute"
                "recorder"
                "pan flute"
                "blown bottle"
                "shakuhachi"
                "whistle"
                "ocarina"
                "square lead"
                "saw lead"
                "calliope lead"
                "chiff lead"
                "charang lead"
                "voice lead"
                "fifths lead"
                "bass lead"
                "new age pad"
                "warm pad"
                "polysynth"
                "choir pad"
                "bowed pad"
                "metallic pad"
                "halo pad"
                "sweep pad"
                "rain"
                "soundtrack"
                "crystal"
                "atmosphere"
                "brightness"
                "goblins"
                "echoes"
                "sci-fi"
                "sitar"
                "banjo"
                "shamisen"
                "koto"
                "kalimba"
                "bagpipes"
                "fiddle"
                "shanai"
                "tinkle bell"
                "agogo"
                "steel drums"
                "woodblock"
                "taiko"
                "melodic tom"
                "synth drum"
                "reverse cymbal"
                "guitar fret noise"
                "breath noise"
                "seashore"
                "bird tweet"
                "telephone"
                "helicopter"
                "applause"
                "gunshot"
            }
            
            set ins [lsearch $instruments $string]

            if {$ins > -1} {

                return $ins

            } else {

                error "instrument not found"

            }

        }

        proc drum {string} {

            set drums {
                "acoustic bass drum" 35
                "bass drum" 36
                "side stick" 37
                "acoustic snare" 38
                "hand clap" 39
                "electric snare" 40
                "low floor tom" 41
                "closed hat" 42
                "high floor tom" 43
                "pedal hat" 44
                "low tom" 45
                "open hat" 46
                "low mid tom" 47 
                "high mid tom" 48
                "crash cymbal" 49
                "high tom" 50
                "ride cymbal" 51
                "chinese cymbal" 52
                "ride bell" 53
                "tambourine" 54
                "splash cymbal" 55
                "cowbell" 56
                "crash cymbal 2" 57
                "vibraslap" 58
                "ride cymbal 2" 59
                "high bongo" 60
                "low bongo" 61
                "mute high conga" 62
                "open high conga" 63
                "low conga" 64
                "high timbale" 65
                "low timbale" 66
                "high agogo" 67
                "low agogo" 68
                "cabasa" 69
                "maracas" 70
                "short whistle" 71
                "long whistle" 72
                "short guiro" 73
                "long guiro" 74
                "claves" 75
                "high woodblock" 76
                "low woodblock" 77
                "mute cuica" 78
                "open cuica" 79
                "mute triangle" 80
                "open triangle" 81
            }

            return [dict get $drums $string]

        }

        proc note {class octave} {

            set notes {
                "c" 0
                "c#" 1
                "db" 1
                "d" 2
                "d#" 3
                "eb" 3
                "e" 4
                "f" 5
                "f#" 6
                "gb" 6               
                "g" 7
                "g#" 8
                "ab" 8
                "a" 9
                "a#" 10
                "bb" 10
                "b" 11
            }

            return [expr {
                [dict get $notes [string tolower $class]] + ($octave * 12)
            }]

        }

        proc chord {root quality} {
            
            set types {
                "" {0 4 7}
                "M" {0 4 7}
                "maj" {0 4 7}
                "m" {0 3 7}
                "min" {0 3 7}
                "7" {0 4 7 10}
                "M7" {0 4 7 11}
                "maj7" {0 4 7 11}
                "m7" {0 3 7 10}
                "min7" {0 3 7 10}
                "dim7" {0 3 6 9}
                "add9" {0 4 7 14}
                "Madd9" {0 4 7 14}
                "madd9" {0 3 7 14}
            }

            set outlist {}

            foreach note [dict get $types $quality] {

                lappend outlist [expr {$root + $note}]

            }

            return $outlist

        }
                
    }

    # list processing commands
    namespace eval List {

        namespace export {*}{
            lrotate
            lrandom
            lremove
            lpops
        }

        # rotate a list
        # if amount is positive, |amount| items are moved from the end of the list to the front
        # if amount is negative, |amount| items are moved from the front of the list to the end
        # if |amount| is greater than the length of the list, |amount| % |list_length| is used
        proc lrotate {list amount} {
            
            if {$amount == 0} {

                return $list

            } elseif {$amount < 0} {

                set rotate_amount [% [abs $amount] [llength $list]]
                return [list \
                    {*}[lrange $list $rotate_amount end] \
                    {*}[lrange $list 0 [- $rotate_amount 1]]\
                ]

            } elseif {$amount > 0} {

                return [lreverse [lrotate [lreverse $list] -$amount]]  

            }

        }

        # return a random list element
        proc lrandom {list} {

            return [lindex $list [expr {int(rand()*[llength $list])}]]

        }
        
        # return a copy of a list with an element removed
        proc lremove {list index} {

            return [lreplace $list $index $index]

        }

        # remove in-place the first element of a list and return it
        proc lpops {list_name} {

            upvar $list_name list
            set first [lindex $list 0]
            set list [lremove $list 0]

            return $first

        }

    }

    # extra control flow commands
    namespace eval Flow_control {

        namespace export {*}{
            repeat
        }

        proc repeat {times var code} {

            upvar $var i

            for {set i 0} {$i < $times} {incr i} {
 
               uplevel $code

            }

        }

    }

    # extra math commands
    namespace eval Math {

        namespace export {*}{
            intrand
        }

        proc intrand {min max} {

            return [expr {int(rand()*($max-$min+1)+$min)}]

        }

    }

    # allows us to send commands into the future and execute them implicitly
    # with a special version of the wait command
    namespace eval Oracle {

        namespace export {*}{
            oracle
                foretell
                wait
        }

        # this variable holds a bunch of {time command} pairs
        variable future
        set future {}

        # get the time of an event
        proc time {event} {
        
            return [lindex $event 0]

        }

        # get the command associated with an event
        proc command {event} {
        
            return [lindex $event 1]

        }

        # add an event to the list
        proc foretell {time command} {

            variable future
            lappend future [list $time $command]

        }

        # get the number of ticks until the most imminent event in the list
        proc time_until_next_foretold_event {} {
            
            variable future
            
            set winner [lindex $future 0]
            
            foreach event $future {
                
                if {[time $event] < [time $winner]} {
                    set winner $event
                }

            }

            return [time $winner]

        }

        # this is like normal wait
        # but `uplevel`s all the commands associated with events in the list
        proc wait {time} {

            variable future

            set time_remaining $time

            while {$time_remaining > 0} {

                # just wait out remaining time if there are no events left to process
                if {[llength $future] == 0} {
                    ::wait $time_remaining
                    break
                }

                set new_future {}

                # either wait until next event or wait all the remaining time
                set next_time [::tcl::mathfunc::min  \
                    [time_until_next_foretold_event] \
                    $time_remaining]

                incr time_remaining "-$next_time"
                ::wait $next_time

                foreach event $future {

                    set new_time [time $event]
                    set command [command $event]
                    incr new_time "-$next_time"

                    if {$new_time > 0} {

                        lappend new_future [list $new_time $command]

                    } else {

                        uplevel $command

                    }
                
                }

                set future $new_future

            }
                
        }

        namespace ensemble create -command oracle

    }

    # useful helper commands related to music composition
    namespace eval Extra {

        namespace export {*} {
            rhythm
                lhxy
            markov
            player
        }

        # contains commands to generate rhythms
        # imported by default into extra
        namespace eval Rhythms {

            namespace export {*}{
                rhythm  
                    lhxy
            }

            # simply returns all of the items in $blocks that have length $length
            proc _bl {blocks length} {
                
                set outrhythms {}

                foreach block $blocks {

                    if {[llength $block] == $length} {

                        lappend outrhythms $block

                    }

                }

                return $outrhythms

            }

            # returns all the rhythms that can be made by
            # combining $hits $blocks to get a pattern with length $length
            proc _blh {blocks length hits} {

                if {$hits == 1} {

                    return [_bl $blocks $length]

                }

                if {$length < 1} { 

                    return {}

                }

                set outrhythms {}

                # append to each block
                # all of the rhythms that can be made with the same blocks that:
                # have a length of length-(length of block)
                # have hits-1 hits
                foreach block $blocks {

                    set tail_length [- $length [llength $block]]
                    set hit_count [- $hits 1]

                    foreach tail [_blh $blocks $tail_length $hit_count] {

                        lappend outrhythms [list {*}$block {*}$tail]

                    }

                }

                return $outrhythms

            }

            # returns all the possible rhythms:
            # of length $length
            # containing hits $hits
            # where hits are spaced of a different between min_block and max_block
            # where min_block == 1, hits can be adjacent
            # where min_block == 2, hits will be separated by 
            proc lhxy {length hits min_block max_block} {
                
                if {$min_block < 1} {

                    return {}

                }

                if {$min_block > $max_block} {

                    return {}

                }

                set blocks {}

                # create the blocks
                for {set i $min_block} {$i <= $max_block} {incr i} {

                    lappend blocks [list 1 {*}[lrepeat [tcl::mathop::- $i 1] 0]]

                }

                # get the base rhythms
                set rhythms [_blh $blocks $length $hits]
                set outrhythms {} 

                # rotate the base rhythms in every combination
                for {set i 0} {$i < $length} {incr i} {

                    foreach rhythm $rhythms {

                        set rotated_rhythm [lrotate $rhythm $i]

                        # and add them them to the output list if they aren't already included
                        # (some base rhythms may be rotations of other base rhythms)
                        if {[lsearch -exact $outrhythms $rotated_rhythm] == -1} {

                            lappend outrhythms $rotated_rhythm

                        }

                    }

                }

                return $outrhythms

            }

            # create an ensemble command for everything in here
            namespace ensemble create -command rhythm

        }

        # a command for simple markov action
        # select a new state based on the current state and a table of probabilities
        proc markov {current_state probs} {

            set current_probs [dict get $probs $current_state]
            set total_weight 0

            foreach key [dict keys $current_probs] {

                incr total_weight [dict get $current_probs $key]

            }

            set target_weight [expr {int(rand() * $total_weight)}]
            set current_weight 0

            foreach state [dict keys $current_probs] {

                incr current_weight [dict get $current_probs $state]

                if {$current_weight >= $target_weight} {

                    return $state

                }

            }

        }

        # another layer of abstraction over Midi_asm
        # using OO this time
        # allows us to more simply represent one musician/instrument
        oo::class create player {

            constructor {init_channel init_program} {

                my variable channel
                my variable volume
                my variable program
                my variable note_offs
                
                set channel $init_channel        
                set volume 100
                set program $init_program
                set note_offs {}

            }

            method note_on {note {velocity -1}} {

                my variable channel
                my variable volume
                my variable program
                my variable note_offs

                program_change $channel $program
                lappend note_offs $note

                if {$velocity < 0} {

                    note_on $channel $note $volume

                } else {

                    note_on $channel $note $velocity

                }

            }

            method all_notes_off {} {

                my variable note_offs
                my variable channel

                foreach note $note_offs {

                    note_off $channel $note

                }

                set note_offs {}

            }

            method set_instrument {new_program} {

                my variable program
                set program $new_program

            }

            method set_volume {new_volume} {

                my variable volume
                set volume $new_volume

            }

        }
	
		# stuff for non-12tet music
		namespace eval microtonal {

			namespace export {*}{
				from_hz
				freq_on
				freq_off
				jix
				freqx
			}

			proc from_hz {freq} {
			
				# todo put these somewhere else
				set base 440.0
				set base_note 69
				set pbr 4
				
				set centicents [expr {12*(log(double($freq)/$base) / log(2))}]
				set note_offset [expr {floor($centicents)}]
				set pitch_offset [expr {$centicents - $note_offset}]
				
				set note [expr {int(69+$note_offset)}]
				set pitch_bend [expr {int((16384.0/$pbr)*$pitch_offset)}]
				
				return "$note $pitch_bend"
				
			}
			
			proc freq_on {channel freq velocity} {

				set note [lindex [from_hz $freq] 0]
				set bend [lindex [from_hz $freq] 1]
				
				pitch_bend $channel $bend
				note_on $channel $note $velocity
				
			}

			proc freq_off {channel freq} {

				set note [lindex [from_hz $freq] 0]
				note_off $channel $note

			}
			
			proc jix {args} {
			
				set top 1
				set bot 1
				
				foreach r $args {
					regexp {([0-9]+)/([0-9]+)} $r -> rt rb
					set top [expr {$top*$rt}]
					set bot [expr {$bot*$rb}]
				}
				
				return "$top/$bot"
			}
		
			proc freqx {freq frac} {
			
				regexp {([0-9]+)/([0-9]+)} $frac -> rt rb
				return [expr {$freq*double($rt)/double($rb)}]
				
			}

		}
		
		
        # import everything useful into extra
        namespace import Rhythms::rhythm
		namespace import microtonal::*

    }

    # import commonly used namespaces
    namespace import tcl::mathop::*
    namespace import tcl::mathfunc::*
    namespace import Comms::*
    namespace import Midi_asm::*
    namespace import Midi_defs::*
    namespace import List::*
    namespace import Flow_control::*
    namespace import Math::*
    namespace import Oracle::oracle
    namespace import Extra::*
	namespace import

    # initialise shared variables
    __comms_set midi_bytes ""
    __comms_set midi_header ""
    __comms_set midi_track_chunk_header ""
    __comms_set ppq_set 0
    __comms_set ppq 96
    __comms_set delta 0

    # initialize song
    tempo 100

}

# this is run inside the user interpreter after the user code
set shutdown {

    # calculate the header and track chunk header
    __comms_set midi_header [Midi::get_header 0 1 [__comms_get ppq]]
    __comms_set midi_track_chunk_header [Midi::get_track_chunk_header\
        [expr {[string length [__comms_get midi_bytes]] + 4}]]

    # add "end of track" message
    __comms_append midi_bytes [Midi::end_of_track [__comms_get delta]]

}

# hopefully we have cmdline, but if not 
if [catch {package require cmdline}] {

    # just read from stdin and write to stdout
    set code [read stdin]
    safe_eval "$startup \n\n $code \n\n $shutdown"
    fconfigure stdout -translation binary
    puts -nonewline stdout "[__comms_get midi_header][__comms_get midi_track_chunk_header][__comms_get midi_bytes]"

# if we DO have cmdline
} else {

    # set up the cmdline options
    set options {
        {i.arg "" "The input file to read from"}
        {o.arg "" "The output file to write to"}
    }

    set usage "- MIDI Assembler v0.0:\n\nHopefully you have received Readme.md or Readme.html with this file.\nPlease refer to those for more detailed help.\n"

    # try to get the options
    # print the usage info if something goes wrong
    if [catch {array set params [::cmdline::getoptions argv $options $usage]}] {

        puts [::cmdline::usage $options $usage]
        exit

    }

    # read input file if we have it
    if [expr [string length $params(i)] > 0] {

        if [catch {set ifh [open $params(i) r]}] {

            puts "couldn't open file $params(i)"
            exit

        }

        set code [read $ifh]
        close $ifh

    # otherwise use stdin               
    } else {

        set code [read stdin]

    }

    # write to output file if we have it
    # TODO: confirm before overwriting if file exists
    if [expr [string length $params(o)] > 0] {

        if [catch {set ofh [open $params(o) w]}] {

            puts "couldn't create file $params(o)"
            exit

        }

        safe_eval "$startup \n\n $code \n\n $shutdown"
        fconfigure $ofh -translation binary
        puts -nonewline $ofh "[__comms_get midi_header][__comms_get midi_track_chunk_header][__comms_get midi_bytes]"
        close $ofh

    # otherwise use stdout               
    } else {

        safe_eval "$startup \n\n $code \n\n $shutdown"
        fconfigure stdout -translation binary
        puts -nonewline stdout "[__comms_get midi_header][__comms_get midi_track_chunk_header][__comms_get midi_bytes]"

    }

}

