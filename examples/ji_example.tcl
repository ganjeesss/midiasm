proc ... {t} { tailcall oracle wait $t }

proc ! {ratio} {
    set hz [freqx 294 $ratio]
    freq_on 0 $hz 100
    oracle foretell 48 "freq_off 0 $hz"
}

proc !! {ratios} {
    foreach r $ratios {
        ! $r
        ... 96
    }
}

set d  1/1
set a  3/2
set e  [jix 3/2 3/2]
set b  [jix 3/2 3/2 3/2]
set fx [jix 3/2 3/2 3/2 3/2]

!! [list $d $a $e $b $fx [jix $fx 10/9]]