tempo 40

proc arp {player speed args} {
    set i 0
    foreach note $args {
        oracle foretell $i "$player note_on $note"
        incr i $speed
    }
}


set gary [player new 0 [instrument "jazz guitar"]]

repeat 8 __1 {
    arp $gary 12 [note c 5] [note e 5] [note g 5]
    oracle foretell 48 {$gary all_notes_off}
    
    oracle wait 96
}


