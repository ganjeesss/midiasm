proc is_hit {instrument i} {
    upvar "${instrument}_rhythm" rhythm
    return [lindex $rhythm $i]
}

proc volume {instrument} {
    upvar "${instrument}_volume" vol
    return $vol
}

proc is_mute {instrument} {
    upvar "${instrument}_mute" mute
    return $mute
}

proc mute {instrument} {
    upvar "${instrument}_mute" mute
    set mute 1
}

proc unmute {instrument} {
    upvar "${instrument}_mute" mute
    set mute 0
}

proc patch {instrument} {
    upvar "${instrument}" patch
    return $patch
}

proc markovian_rhythm {length probs} {
    set rhythm {}
    set state 0
    
    repeat $length __rhythm_make {
        set state [markov $state $probs]
        lappend rhythm $state
    }
    
    return $rhythm
}

proc play_chord {player chord} {
    set note [string range $chord 0 0]
    set quality [string range $chord 1 end]

    set root [note $note 5]
 
    foreach note [chord $root $quality] {
        $player note_on $note
    }
}

proc random_chord_note {player chord} {
    set note [string range $chord 0 0]
    set quality [string range $chord 1 end]

    set root [note $note 5]
    $player note_on [lrandom [chord $root $quality]]
}

proc play_bass_note {player chord} {
    set note [string range $chord 0 0]
    set root [note $note 5]
 
    $player note_on [- $root 24]
}

set drummer [player new 9 1]
set keyboardist [player new 1 [intrand 0 90]]
set bassist [player new 2 [intrand 32 39]]

$keyboardist set_volume 50
$bassist set_volume 70

set kick [drum "bass drum"]
set hibong [drum [lrandom {"low mid tom" "high bongo" "high timbale" "high agogo" "high woodblock" "mute cuica" }]]
set lobong [+ $hibong 1]
set tap1 [drum [lrandom {"low mid tom" "high bongo" "high timbale" "high agogo" "high woodblock" "mute cuica" }]]
set tap2 [+ $tap1 1]

set kick_volume 127
set hibong_volume 80
set lobong_volume 80
set tap1_volume 50
set tap2_volume 50
set thing_volume 100

set bong_probs { 0 { 1 40 0 60 } 1 { 1 0 0 100 } }
set tap_probs { 0 { 1 50 0 50 } 1 { 1 20 0 80 } }
set chord_rhythm_probs { 0 { 1 30 0 70 } 1 { 1 0 0 100 } }

proc make_rhythms {} {
    uplevel {

        if {[intrand 0 100] > 90} {
            set kick_rhythm {1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1}
        } else {
            set kick_rhythm {1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0}
        }

        if {[intrand 0 100] > 60} {
            set bass_rhythm [markovian_rhythm 16 $bong_probs]
        } else {
            set bass_rhythm {0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0}
        }

        if {[intrand 0 100] > 90} {
            set thing_rhythm {1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1} 
        } else {
            set thing_rhythm {0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0}
        }

        set hibong_rhythm [markovian_rhythm 16 $bong_probs]
        set lobong_rhythm [lrotate $hibong_rhythm 3]
        set tap1_rhythm [markovian_rhythm 16 $tap_probs]
        set tap2_rhythm [lrotate $tap1_rhythm 2]
        set chord_rhythm [markovian_rhythm 16 $chord_rhythm_probs]

    }
}
make_rhythms

proc set_thingstrument {} {
    uplevel {
        set thing [drum [lrandom {
            "acoustic bass drum" 
            "bass drum" 
            "side stick" 
            "acoustic snare" 
            "hand clap" 
            "electric snare" 
            "low floor tom" 
            "closed hat" 
            "high floor tom" 
            "pedal hat" 
            "low tom" 
            "open hat" 
            "low mid tom"  
            "high mid tom" 
            "crash cymbal" 
            "high tom" 
            "ride cymbal" 
            "chinese cymbal" 
            "ride bell" 
            "tambourine" 
            "splash cymbal" 
            "cowbell" 
            "crash cymbal 2" 
            "vibraslap" 
            "ride cymbal 2" 
            "high bongo" 
            "low bongo" 
            "mute high conga" 
            "open high conga" 
            "low conga" 
            "high timbale" 
            "low timbale" 
            "high agogo" 
            "low agogo" 
            "cabasa" 
            "maracas" 
            "short whistle" 
            "long whistle" 
            "short guiro" 
            "long guiro" 
            "claves" 
            "high woodblock" 
            "low woodblock" 
            "mute cuica" 
            "open cuica" 
            "mute triangle" 
            "open triangle" 
        }]]
    }
}

proc set_instruments {} {
    uplevel {
        set kick [drum "bass drum"]
        set hibong [drum [lrandom {"low mid tom" "high bongo" "high timbale" "high agogo" "high woodblock" "mute cuica"}]]
        set lobong [+ $hibong 1]
        set tap1 [drum [lrandom {"low mid tom" "high bongo" "high timbale" "high agogo" "high woodblock" "mute cuica"}]]
        set tap2 [+ $tap1 1]
        set_thingstrument


        $keyboardist set_instrument [intrand 0 90]
        $bassist set_instrument [intrand 32 39]
    }
}
set_instruments

proc do_muting {} {
    uplevel {
        foreach thing {
            kick
            hibong
            lobong
            tap1
            tap2
            thing
            chord
            bass
        } {
            unmute $thing
            if {[intrand 0 100] > 60} {
                mute $thing
            }
        }
    }
}
do_muting

set chord_probs {
    CM7   { Dm7 10  Em7 20  FM7 20  Gadd9 20    Am7 20 }
    Dm7   { CM7 10  Em7 10  FM7 20    Em7 10  Gadd9 50 }
    Em7   { CM7 30  Dm7 00  FM7 40    Am7 30  Gadd9 00 }
    FM7   { CM7 30  Dm7 10  Em7 00  Gadd9 30    Am7 30 }
    Gadd9 { CM7 80  Dm7 00  Em7 00    FM7 00    Am7 20 }
    Am7   { CM7 20  Dm7 20  Em7 20    FM7 20  Gadd9 20 }
}

set current_chord CM7

repeat 16 __phase {

    tempo [intrand 80 160]
    make_rhythms
    do_muting
    set_instruments

    if {[intrand 0 100] > 20} {
        $drummer note_on [drum [lrandom {
            "crash cymbal"
            "ride cymbal"
            "splash cymbal"
            "chinese cymbal"
            "vibraslap"
        }]]
    }

    repeat 8 __bar {

        if {[intrand 0 100] > 60} {
            $drummer note_on [drum [lrandom {
                "crash cymbal"
                "ride cymbal"
                "splash cymbal"
                "chinese cymbal"
                "vibraslap"
            }]]
        }

        repeat 16 __step {

            if {[intrand 0 100] > 90} {
                $drummer note_on [drum [lrandom {
                    "crash cymbal"
                    "ride cymbal"
                    "splash cymbal"
                    "chinese cymbal"
                    "vibraslap"
                }]]
            }

            foreach drum {
                kick
                hibong
                lobong
                tap1
                tap2
                thing
            } {
                if { [is_hit $drum $__step] && ![is_mute $drum] } {
                    $drummer note_on [patch $drum] [volume $drum]
                }
            }

            if { [is_hit chord $__step] && ![is_mute chord] } {
                if {[intrand 0 100] > 50} {
                    random_chord_note $keyboardist $current_chord
                } else {
                    play_chord $keyboardist $current_chord
                }
            }

            if { [is_hit bass $__step] && ![is_mute bass] } {
                play_bass_note $bassist $current_chord
            }

            wait 24
            if { [intrand 0 100] > 98 } {
                wait 12
            }

            $drummer all_notes_off
            $keyboardist all_notes_off
            $bassist all_notes_off

        }

        if {[intrand 0 100] > 85} {
            unmute thing
            set_thingstrument
            set thing_rhythm {1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1} 
        } elseif {[intrand 0 100] > 85} {
            unmute thing
            set_thingstrument
            set thing_rhythm {0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0}

        } else {
            set thing_rhythm {0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0}
        }

        set current_chord [markov $current_chord $chord_probs]

    }

    wait 96

}
