for {set i 0} {$i < 127} {incr i} {
    tempo [+ $i 40]
    program_change 0 $i
    note_on 0 $i 100
    wait 96
    note_off 0 $i
}
