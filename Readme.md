# Programming and Music with "MIDI assembler"

## In this manual:

* What is it

* Getting it

* Running it

* Writing MIDI assembler scripts

* MIDI commands

* Other commands

* Math operators

* Examples

## What is it

MIDI assembler is a "sandbox" type programming environment specifically for creating [MIDI files](https://en.wikipedia.org/wiki/Musical_Instrument_Digital_Interface#Standard_MIDI_files). MIDI files are tiny files that all different kinds of computers and synthesizers can use to play back music - it's a standardized format that's been around for ages (think *Age of Empires* soundtrack).

MIDI assembler uses Tcl as the language both for the implementation itself and as the language the user (that's you) uses to create MIDI files. User code is run in a "safe" interpreter so file I/O, arbitrary code execution and so forth aren't possible from within scripts (unless you want them to be).

If you're thinking of learning to program in Tcl, MIDI assembler could be a gentle and immediately exciting introduction - it sure beats making "Hello, world!" appear on a terminal display, or calculating the fibonacci sequence in six different ways.

## Getting it

MIDI assembler can be downloaded along with this file and some examples at [bitbucket](http://bitbucket.com/ganjeesss/midi-assembler).

## Running it

MIDI assembler is written in [Tcl](http://tcl.tk/), a programming language created by John Ousterhout in 1988. No native code is used which means the MIDI assembler *should* run on any platform supported by Tcl.

* MS Windows: running Tcl programs on Microsoft Windows is easy. Option 1: Download and install the free [ActiveTcl](http://www.activestate.com/activetcl/downloads) package, which gives you a full Tcl environment on windows. Option 2: Download [FreeWrap](http://freewrap.sourceforge.net/). This is a single executable which can bundle Tcl scripts into Windows executables. ActiveTcl is surely the better option, but FreeWrap may be simpler.

* Mac OSX: [ActiveTcl](http://www.activestate.com/activetcl/downloads) is available on Mac OSX.

* GNU+Linux/BSD: running Tcl programs on GNU+Linux/BSD is easy. Many distros will come with Tcl installed by default, and almost all will have it available in the repositories, or if not, [ActiveTcl](http://www.activestate.com/activetcl/downloads) is also available for x86 Linux.

MIDI assembler is operated via a command-line interface.

* On Windows, if using ActiveTcl, make sure the ActiveTcl binary folder is in your path, and then use `type script.txt | tclsh midiasm.tcl > out.mid` in a command prompt in the same folder that you saved midiasm.tcl to, where script.txt is a saved script and out.mid is the output file.

* On Windows, if using FreeWrap, compile midiasm.tcl with freewrapTCLSH.exe to create a command-line version, and then run with `type script.txt | midiasm.exe > out.mid` (see above).

* On GNU+Linux/BSD (and probably OSX too), with Tcl installed, run in a terminal `tclsh midiasm.tcl < script.txt > out.mid`.

If you have tcllib installed (which is a part of ActiveTcl, and should be easy enough to install on other platforms), you can also specify your input and output files directly on the command-line, e.g. `tclsh midiasm.tcl -i script.txt -o out.mid`. Type `midiasm.tcl -help` for more information - if you don't have tcllib installed, nothing will happen, but you can still use the program as long as you use |, \< and \> to get files into and out of MIDI assembler.

## Writing MIDI assembler scripts

MIDI assembler scripts are written in plain Tcl. An advantage of this is that there is already a lot of documentation and a large community focusing on the language itself. On GNU+Linux/BSD, or with ActiveTcl installed, you should be able to follow through any [existing Tcl tutorials](http://www.tcl.tk/man/tcl8.5/tutorial/tcltutorial.html) before using MIDI assembler if you want. Although it might seem daunting, only really a rudimentary knowledge of Tcl is required to write MIDI assembler scripts, so don't feel obliged to finish the whole tutorial.

A basic knowledge of the MIDI format would help too, but isn't that important.

Some commands have been added to the language for MIDI assembler, these are detailed below.

## Basic MIDI commands

These commands relate to the MIDI file format - the important bit. A MIDI file is made up of a series of events separated from each other by periods of time. Various kinds of event are available, such as to turn notes on and off, change instruments and tempo, and so on, and these events can be spread out over 16 different channels (imagine each channel represents one player in a band), numbered from 0 to 15. Channel 9 is always the drum kit, and every other channel represents a melodic instrument.

MIDI also has a variety of events that can be used to control hardware and software synthesizers and add detailed effects to music - these are supported in MIDI assembler too, but the most important commands are the following five:

### wait

This command takes one argument, the number of ticks to wait:

    wait 96
    
will wait 96 ticks, which is (by default) one quarter-note.

### note_on

This command takes three arguments, the channel to play the note on (0..15), the note number (0..127), and the velocity (0..127):

    note_on 0 60 100
    
will play note 60 (middle C) on channel 0 with a velocity of 100.

### note_off

This command takes two arguments, the channel (0..15) and note number (0..127) of the note to turn off:

    note_off 0 60
    
will stop a currently playing note 60 on channel 0 from sounding.
    
would bend as far down as possible on channel 0. The maximum with of the pitch bend effect depends on the device playing back the MIDI file, and can sometimes be configured with CC control.

### program_change

This command takes two arguments, the channel (0..15) and the patch (0..127) to change to:

    program_change 0 10

will change the instrument on channel 0 to instrument 11 (music box). A list of which numbers correspond to which instruments can be found [here](http://www.midi.org/techspecs/gm1sound.php), note that instrument numbers start from 0 in MIDI assembler and from 1 in the GM specs, which is why the above command changes to instrument 11, rather than 10.

### tempo

This command takes one argument, the tempo in beats per minute:

    tempo 150
    
will set the tempo to 150 beats per minute.

## Advanced MIDI commands

These commands give you more control over the synthesizers and software you might be using to play back your MIDI files. I won't explain them in too much detail here, as they are fairly self-explanatory if you're familiar with the MIDI format.

### pitch_bend

This command takes two arguments, the channel (0..15) and the amount (-8192..8191) to bend by:

    pitch_bend 0 -8192

### n_touch

This command ("note touch") takes three arguments, the channel (0..15), note (0..127) and pressure (0..127) to register aftertouch with:

    n_touch 0 70 100
   
will change key pressure on note 70 on channel 0 to 100.

### c_touch

This command ("channel touch") takes two arguments, the channel (0..15) and pressure (0..127) to register aftertouch with:

    c_touch 0 100

will change key pressure on channel 0 to 100.

### cc

This command takes three arguments, the channel (0..15), the controller number (0..127) and the value (0..127) to set it to:

    cc 0 10 127
    
will set controller 10 (pan) to 127. A list of which controllers do what can be found [here](http://www.indiana.edu/~emusic/cntrlnumb.html), although not all of them may be implemented by the device you are using for playback. If you are commonly using certain controllers you could make your own commands for them:

    proc pan {channel amount} { cc $channel 10 $amount }

### ppq

This command takes one argument, the parts (ticks) per quarter of the song:

    ppq 96
    
will set the parts per quarter to 96 - this means `wait 96` will wait for one quarter-note. This command can only be used once, if you include it in your script more than once, all invocations besides the first will be ignored - the ppq cannot be changed mid-song.

### sysex

This command takes one or more arguments which are all binary strings (as produced by [binary format](http://www.tcl.tk/man/tcl8.6/TclCmd/binary.htm)) and creates a System Exclusive message with them as the data:

    sysex [binary format c* {25 60 37 80 48 3}] [binary format c* {47 24 93 12 49}]
    
will create a two-part sysex message with the numbers 25, 60, 37, 80, 48, 3 as the first block of data and 47, 24, 93, 12, 49 as the second block of data.

## Other commands

MIDI assembler also has some added commands for convenience that aren't directly related to MIDI output.

### debug

This command takes one argument and prints it to stderr:

    debug "hello, world!"

would print "hello, world!" to stderr.

### slurp

This command loads a text file into MIDI assembler:

    set data [slurp textfile.txt]
    debug $data

would print the contents of textfile.txt to stderr. The file you slurp must be in (or in a subdirectory of) your current working directory (the directory you're running MIDI assembler from).

### source

This command [exists in Tcl](http://www.tcl.tk/man/tcl8.6/TclCmd/source.htm), but was reimplemented for the MIDI assembler. In MIDI assembler, `source` simply `slurp`s a file and runs it as part of your script:

    source textfile.txt

would run the Tcl code in textfile.txt as if it were part of your script    . The `slurp` rule about file location applies here too.

### instrument

    instrument name

to be documented

### drum

    drum name

tbd

### note

    note octave

tbd

### chord

    chord root quality

tbd

### List commmands

    lrotate list amount
    lrandom list
    lremove list index
    lpops list_name

tbd

### repeat

    repeat times var code

tbd

### intrand

    intrand min max

tbd

### Oracle commands

    oracle foretell time command
    oracle wait time

tbd


### Rhythm commmands

    rhythm lhxy length hits x y

tbd

### markov

    markov state probs

tbd

### Player class

tbd

## Math operators

Additionally, the math functions from [::tcl::mathop](http://www.tcl.tk/man/tcl8.6/TclCmd/mathop.htm) and [::tcl::mathfunc](http://www.tcl.tk/man/tcl8.6/TclCmd/mathfunc.htm) have been made available in the global namespace, which means you can do maths [directly with them](https://en.wikipedia.org/wiki/Polish_notation):

    [+ 40 13]
    [int [/ 20 8]]
    
instead of using [expr](http://www.tcl.tk/man/tcl8.6/TclCmd/expr.htm).

## Examples

### example 1

This is a program that plays middle C.

    note_on 0 60 100
    wait 96
    note_off 0 60

### example 2

This is a program that plays all of the notes from 0 to 127, each on a different instrument:

    for {set i 0} {$i < 127} {incr i} {
        tempo [+ $i 40]
        program_change 0 $i
        note_on 0 $i 100
        wait 96
        note_off 0 $i
    }
    
The for loop increments the variable i from 0 to 127, each time:
    
* setting the tempo to [+ $i 40] (so the tempo will range from 40bpm to 167bpm)

* changing the current instrument on channel 0 to $i, (so the instrument will range from 0 to 127)

* playing a note (with velocity 100), waiting for 96 ticks (one quarter-note), and turning the note off again.

